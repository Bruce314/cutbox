#! /usr/bin/env python
'''
Copyright (C) 2014-2015 Florent Revelut <florent@revelut.ch>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

__version__ = "0.1"

import inkex, simplestyle
from simplepath import formatPath
import logging
import operator
import os.path
import itertools as IT
import tempfile
import traceback as TB

#                     Thickness
#                    <->
#    +---+ +-+ +-+ +---+ -
#    |   | | | | | |   | thickness
#    |   +-+ +-+ +-+   | -
#    +-+  ^          +-+
#      |  |          |
#    +-+<-+-width--->+-+
#    |    |            |
#    +-+  height     +-+
#      |  |          |
#    +-+  v  BOT     +-+
#    |   +-+ +-+ +-+   |
#    |   | | | | | |   |
#    +---+ +-+ +-+ +---+


# useful note : if inkscape is stuck, try to see if process never returned and kill it
# ps aux | grep CutBox | grep python | cut -c8-14 | xargs kill

class CutBox(inkex.Effect):

  def __init__(self):
    inkex.Effect.__init__(self)
    logging.info ("init start")
    #super(CutBox,self).__init__()
    self.OptionParser.add_option("-x", "--width",
                                 action="store", type="float",
                                 dest="width", default=10.0,
                                 help="The Box Width - in the X dimension")
    self.OptionParser.add_option("-y", "--height",
                                 action="store", type="float",
                                 dest="height", default=15.0,
                                 help="The Box Height - in the Y dimension")
    self.OptionParser.add_option("-z", "--depth",
                                 action="store", type="float",
                                 dest="depth", default=3.0,
                                 help="The Box Depth - in the Z dimension")
    self.OptionParser.add_option("-t", "--thickness",
                                 action="store", type="float",
                                 dest="thickness", default=0.3,
                                 help="Material thickiness")

    self.OptionParser.add_option("-r", "--hinge-ratio",
                                 action="store", type="float",
                                 dest="hingeRatio", default=2.0,
                                 help="Hinge ratio")

    self.OptionParser.add_option("-s", "--hinge-size",
                                 action="store", type="float",
                                 dest="hingeSize", default=0.5,
                                 help="hinge size")

    self.OptionParser.add_option("-d", "--inter-distance",
                                 action="store", type="float",
                                 dest="interDistance", default=1.0,
                                 help="interdistance")

    self.OptionParser.add_option("-u", "--unit",
                                 action="store", type="string",
                                 dest="unit", default="cm",
                                 help="The unit of the box dimensions")

    self.OptionParser.add_option("-k", "--splits",
                                 action="store", type="int",
                                 dest="splits", default="0",
                                 help="number of middle walls")

    self.OptionParser.add_option("-g", "--guide-line",
                                 action="store", type="inkbool",
                                 dest="guideLine", default=True,
                                 help="Add guide lines to help the drawing limits")

    self.OptionParser.add_option("-l", "--lid",
                                 action="store", type="int",
                                 dest="lid", default="1",
                                 help="nature of lid")


    logging.info ("init done")
    (self.DIRECT_X,self.REVERSE_X,self.DIRECT_Y,self.REVERSE_Y)=range(0,4)

  def directionToEnum(self,x):
    retval= abs(x[0])*(0.5-0.5*x[0])+abs(x[1])*(2.5-0.5*x[1])
    return retval
  def antiFunction(self,f):
    r=None
    if f==operator.add:
      r=operator.sub
    elif f==operator.sub:
      r=operator.add
    logging.info ("f:%s - anti(f):%s" %(f,r))
    return r

  def bottom(self):
    # top-left corner
    l = [ [ 'M', [ self.docW -self.boxW/2 - self.thick , self.docH -self.boxH/2 - self.thick    ] ] ]

    # top edge
    l+=self.crank2([self.docW -self.boxW/2 ,self.docH -self.boxH/2-self.thick ],
                   self.crankW,self.DIRECT_X,+1.0)
    # top-right corner
    l+=[ [ 'L', [ self.docW +self.boxW/2 + self.thick , self.docH -self.boxH/2 - self.thick    ] ] ]

    # right edge
    l+=self.crank2([self.docW +self.boxW/2 +self.thick,self.docH -self.boxH/2 ],
                   self.crankH,self.DIRECT_Y,-1.0)

    # bottom-right corner
    l+=[ [ 'L', [ self.docW +self.boxW/2 + self.thick , self.docH +self.boxH/2 + self.thick    ] ] ]

    # bottom edge
    l+=self.crank2([self.docW +self.boxW/2,self.docH + self.boxH/2+self.thick],
                   self.crankW,self.REVERSE_X,-1.0)
    #bottom-left corner
    l+=[ [ 'L', [ self.docW -self.boxW/2 - self.thick , self.docH +self.boxH/2 + self.thick    ] ] ]

    # left edge
    l+=self.crank2([self.docW -self.boxW/2 -self.thick,self.docH +self.boxH/2 ],
                   self.crankH,self.REVERSE_Y,1.0)

    # close line
    l+=[ [ 'Z', [] ] ]
    return l

  def unittouu(self,size):
    try:
      return inkex.Effect.unittouu(self,size)
    except:
      return inkex.unittouu(size)
  def computeBaseValues(self):
    logging.info ("compute Base")
    self.docW = self.unittouu(self.document.getroot().get('width'))/2
    self.docH = self.unittouu(self.document.getroot().get('height'))/2

    self.boxW = self.unittouu( str(self.options.width)  + self.options.unit )
    self.boxH = self.unittouu( str(self.options.height) + self.options.unit )
    self.boxD = self.unittouu( str(self.options.depth)  + self.options.unit )

    self.thick = self.unittouu( str(self.options.thickness)  + self.options.unit )
    self.inter = self.unittouu( str(self.options.interDistance) + self.options.unit )
    self.hinge = self.unittouu( str(self.options.hingeSize) + self.options.unit )

    self.guides=self.options.guideLine

    self.ratio=self.options.hingeRatio
    self.splits=int(self.options.splits)
    self.lid=s=int(self.options.lid)

  def computeCrankPosition(self):
    logging.info ("Compute crank")
    self.repeatD=int(self.boxD/(self.hinge*(1+self.ratio)))
    self.repeatW=int((self.boxW-(2+self.ratio)*self.hinge)/(self.hinge*(1+self.ratio)))
    self.repeatH=int((self.boxH-(2+self.ratio)*self.hinge)/(self.hinge*(1+self.ratio)))
    self.crankH,self.crankW,self.crankD=[],[],[]

    logging.info ("repeatD : %s - repeatW : %s - repeatH : %s" %(self.repeatD,self.repeatW,self.repeatH))
    offs=(self.hinge,self.hinge*self.ratio)

    for (d,ret) in ((self.boxH,self.crankH),(self.boxW,self.crankW),(self.boxD,self.crankD)):
      # Height and width patterns are symmetrical
      logging.info ("Processing d:%s" % (d))
      r=int((d)/(self.hinge*(1+self.ratio)))
      direct=[]
      nondirect=[]
      i=0
      start,end=0,d
      while end-start>min(offs):
        logging.info ("start : %s - end : %s" % (start,end))
        direct+=[start]
        nondirect+=[end]
        start+=offs[i]
        end-=offs[i]
        i=(i+1) % 2
      ret+=direct
      ret+=list(reversed(nondirect))
      logging.info ("%s - %s - %s" % (d,ret,r))
    logging.info ("Crank computed")

  def punch(self,start,crankOffset,direct):
    if type(direct)==int:
      #using enum
      direction=direct
    else:
      #using vector
      direction=self.directionToEnum(direct)
    crunch=self.thick
    trans={self.DIRECT_X:lambda x,y:[start[0]+x,start[1]+crunch*y],
           self.REVERSE_X:lambda x,y:[start[0]-crankOffset[-1]+x,start[1]+crunch*y],
           self.DIRECT_Y:lambda x,y:[start[0]+crunch*y,start[1]+x],
           self.REVERSE_Y:lambda x,y:[start[0]+crunch*y,start[1]-crankOffset[-1]+x]
           }

    if direction in (self.DIRECT_X,self.DIRECT_Y):
      low,high=iter(crankOffset),iter(crankOffset)
    else:
      low,high=iter(reversed(crankOffset)),iter(reversed(crankOffset))
    retVal=[]
    low.next()
    for (sLow,eLow) in IT.izip_longest(low,low):
      logging.debug("%s - %s" %(sLow,eLow))
      if not(eLow) or not(eLow):
        continue
      t1=[['M',trans[direction](sLow,1)]]
      t1+=[['L',trans[direction](sLow,0)]]
      t1+=[['L',trans[direction](eLow,0)]]
      t1+=[['L',trans[direction](eLow,1)]]
      t1+=[['Z',[]]]
      retVal+=[t1]
    return retVal


  def crank2(self,start,crankOffset,direct,crunch):
    """
    crunch is supposed to be [1.0,-1.0]
    """
    if type(direct)==int:
      #using enum
      direction=direct
    else:
      #using vector
      direction=self.directionToEnum(direct)
    trans={self.DIRECT_X:lambda x,y:[start[0]+x,start[1]+crunch*y],
           self.REVERSE_X:lambda x,y:[start[0]-crankOffset[-1]+x,start[1]+crunch*y],
           self.DIRECT_Y:lambda x,y:[start[0]+crunch*y,start[1]+x],
           self.REVERSE_Y:lambda x,y:[start[0]+crunch*y,start[1]-crankOffset[-1]+x]
           }
    t1=[['L',start]]
    if direction in (self.DIRECT_X,self.DIRECT_Y):
      low,high=iter(crankOffset),iter(crankOffset)
    else:
      low,high=iter(reversed(crankOffset)),iter(reversed(crankOffset))
    high.next()
    logging.debug(crankOffset)
    for (sLow,eLow,sHigh,eHigh) in IT.izip_longest(low,low,high,high):
      logging.debug("%s - %s -- %s - %s" %(sLow,eLow,sHigh,eHigh))
      t1+=[['L',trans[direction](sLow,0)]]
      t1+=[['L',trans[direction](eLow,0)]]
      if not(eHigh):
        continue
      t1+=[['L',trans[direction](sHigh,self.thick)]]
      t1+=[['L',trans[direction](eHigh,self.thick)]]
    return t1

  def lateral(self,f,corner):
    """
    sides of the box, will be displayed left and right of the bottom (on Height x Depth)
    """

    # displayed right and left
    # comments are for left
    g=self.box
    sX,sY=corner
    if self.guides:
      self.createGuide( f(sX,self.boxD), 0,90 );
      self.createGuide( f(sX,self.boxD+self.thick), 0,90 );
      self.createGuide( sX, 0,90 );
      self.createGuide( f(sX,-self.thick), 0,90 );

    logging.info ("Lateral : %s"%(f))
    #top left corner
    l =[['M',[sX,sY]]]
    #top edge
    l+=self.crank2([sX,sY], self.crankD,[f(0,1),0],-1)

    l+=[['L',[f(sX,self.boxD),sY]]]
    #right edge
    l+=self.crank2([f(sX,self.boxD),sY],
                   self.crankH,(0,1),f(0,1.0))

    l+=[['L',[f(sX,self.boxD),sY+(self.boxH)]]]
    #bottom edge
    l+=self.crank2([f(sX,self.boxD),sY+(self.boxH)],
                   self.crankD,[f(0,-1),0],+1)
    if self.lid>0:
      # left edge, cranked only if lid
      l+=self.crank2([f(sX,0),sY+self.boxH],
                     self.crankH,[0,-1],f(0,-1))
      if self.guides:
        self.createGuide( f(sX,-self.thick), 0,90 );

    l+=[['Z',[] ]]
    return l
  def lateral2(self,f,corner):
    """
    sides of the box, will be displayed upper and lower than bottom (on Width x Depth)
    """
    g=self.box
    sX=corner[0]
    sY=corner[1]
    if self.guides:
      self.createGuide( 0,f(sY,self.boxD), 0 );
      self.createGuide( 0,f(sY,self.boxD+self.thick), 0 );
      self.createGuide( 0,f(sY,-self.thick), 0 );
      self.createGuide( 0,sY, 0 );

    # description matches for upper piece
    # lower is designed symmetrically
    # lower left corner
    l =[['M',[sX,sY]]]
    #bottom edge
    l+=self.crank2([sX,sY], self.crankW,self.DIRECT_X,f(0,-1.0))

    l+=[['L',[sX+self.boxW,sY]]]
    # right edge
    l+=self.crank2([sX+self.boxW+self.thick,sY], self.crankD,[0,f(0,1)],-1)

    l+=[['L',[sX+self.boxW,f(sY,self.boxD)]]]
    if self.lid>0:
      # top edge, cranked only if lid
      l+=self.crank2([sX+self.boxW,f(sY,self.boxD)],
                     self.crankW,[-1,0],f(0,1))
      if self.guides:
        self.createGuide( 0,f(sY,-self.thick), 0 );

    else:
      l+=[['L',[sX,f(sY,self.boxD)]]]
    # left edge
    l+=self.crank2([sX-self.thick,f(sY,self.boxD)], self.crankD,[0,f(0,-1)],+1)
    l+=[['Z',[] ]]
    return l

  def punchHoles(self):
    if self.splits<1:
      logging.info ("no split!")
      return
    logging.info ("Punching holes!")


  def effect(self):
    logging.info ("go")
    logging.info ("go, options : %s" % (self.options))
    self.computeBaseValues()
    self.computeCrankPosition()

    box_id = self.uniqueId('box')
    self.box = g = inkex.etree.SubElement(self.current_layer, 'g', {'id':box_id})
    line_style = simplestyle.formatStyle({ 'stroke': '#FF0000', 'fill': 'none' })

    l=self.bottom()
    line_atts = { 'style':line_style, 'id':box_id+'-inner-close-tab', 'd':formatPath(l) }
    inkex.etree.SubElement(g, inkex.addNS('path','svg'), line_atts )

    l1 = self.lateral(operator.add,
                      (self.docW-self.boxW/2-self.thick-self.boxD-self.inter,self.docH-self.boxH/2))
    line_atts = { 'style':line_style, 'id':box_id+'-inner-close-tab', 'd':formatPath(l1) }
    inkex.etree.SubElement(g, inkex.addNS('path','svg'), line_atts )
    l1 = self.lateral(operator.sub,
                      (self.docW+self.boxW/2+self.thick+self.boxD+self.inter,self.docH-self.boxH/2))
    line_atts = { 'style':line_style, 'id':box_id+'-inner-close-tab', 'd':formatPath(l1) }
    inkex.etree.SubElement(g, inkex.addNS('path','svg'), line_atts )

    l1 = self.lateral2(operator.add,
                      (self.docW-self.boxW/2,
                       self.docH+self.boxH/2+self.thick+self.inter))
    line_atts = { 'style':line_style, 'id':box_id+'-inner-close-tab', 'd':formatPath(l1) }
    inkex.etree.SubElement(g, inkex.addNS('path','svg'), line_atts )
    l1 = self.lateral2(operator.sub,
                      (self.docW-self.boxW/2,
                       self.docH-self.boxH/2-self.thick-self.inter))
    line_atts = { 'style':line_style, 'id':box_id+'-inner-close-tab', 'd':formatPath(l1) }
    inkex.etree.SubElement(g, inkex.addNS('path','svg'), line_atts )

    logging.info ("Nb of splits : %s" % (self.splits))
    for currentSplit in range(0,self.splits):
      logging.info ("handling split %s/%s" % (currentSplit+1,self.splits))
      xPosition=self.docW-self.boxW/2+(self.boxW*(currentSplit+1))/(self.splits+1)-self.thick/2
      if self.guides:
        self.createGuide( xPosition, 0,90 );
        self.createGuide( xPosition+self.thick, 0,90 );



      l1 = self.punch([xPosition,self.docH-self.boxH/2], self.crankH,(0,1))

      l2 = self.punch((xPosition,
                       self.docH+self.boxH/2+self.thick+self.inter),
                      self.crankD,(0,1))
      l3 = self.punch((xPosition,
                       self.docH-self.boxH/2-self.thick-self.inter),
                      self.crankD,(0,-1))

      for hole in l1+l2+l3:
        line_atts = { 'style':line_style, 'id':box_id+'-inner-close-tab', 'd':formatPath(hole) }
        inkex.etree.SubElement(g, inkex.addNS('path','svg'), line_atts )

    if self.guides:
      self.createGuide( 0, self.docH, 0 );
      self.createGuide( 0, self.docH-self.boxH/2, 0 );
      self.createGuide( 0, self.docH-self.boxH/2-self.thick, 0 );
      self.createGuide( 0, self.docH+self.boxH/2+self.thick, 0 );
      self.createGuide( 0, self.docH+self.boxH/2, 0 );
      self.createGuide( self.docW, 0,90 );
      self.createGuide( self.docW-self.boxW/2, 0,90 );
      self.createGuide( self.docW-self.boxW/2-self.thick, 0,90 );
      self.createGuide( self.docW+self.boxW/2, 0,90 );
      self.createGuide( self.docW+self.boxW/2+self.thick, 0,90 );

if __name__ == '__main__':   #pragma: no cover
  location=os.path.join(tempfile.gettempdir(),'CutBox.log')
  logging.basicConfig(filename=location, level=logging.INFO)
  logging.info('Started')
  try:
    e = CutBox()
    e.affect()
  except Exception as e:
    logging.error("Exception : %s \nTraceback:\n%s" % (str(e),TB.format_exc()))
    raise e
  logging.info('Finished')
