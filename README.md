# README #

This is an inkscape plugin to draw box patterns for laser-cutting.

### What is this repository for? ###

The plugin is written in python. A few tests are provided, tested only in linux.

### How do I get set up? ###

As this is an inkscape plugin, you need to install it in your inkscape install. You will only require CutBox.py (actual code) and CutBox.ink (wrapper for UI)
Copy these files in the correct location:

* OSX / Linux : `~/.config/inkscape/extensions`
* Windows 10 : `%APPDATA%/inkscape/extensions` (it seems it used to
  be `%APPDATA%/.config/inkscape/extensions`, so any of these might be
  true). To install on windows 10, just run the `install_win10.bat`
  file (you might need to run as administrator). It seems that using
  links on windows does not work (using `mklink` or `mklink /H` so you
  need to copy the file).

If correctly installed, you'll find "CutBox" in the Render submenu of
inkscape extensions. If not, check that you got both files in the
folder and restart inkscape.

### Contribution guidelines ###

Feel free to propose features / corrections, I'll happily merge them.

The various parameters are:

- thickness (of material)
- size of the box (note : internal size, so expect +2x tchickness on all dimensions)
- size of the cranks : 2 parameters, base size (for male) and ratio
  for female
- you can pick whether is a closed box (think about a dice) or an open box.

As a note on the design:

- the bottom enforces corners to be male
- guides can be added for further editing
- design should have 2 planar symmetries (on xz and yz plans). for
  assembly, it's better to keep the cutting with a symmetry  rather
  than a translated copy

### Who do I talk to? ###

It's a quick project, written alone so far. Main author is Florent
Revelut (florent@revelut.ch), aka Bruce314. Feel free to request
feature or propose patches.