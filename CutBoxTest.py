import os
import sys

sys.path+=['/usr/share/inkscape/extensions/']

import random
import unittest
import CutBox as CB


class TestConversion(unittest.TestCase):
  def setUp(self):
      self.a=CB.CutBox()
  def test_directx(self):
    self.assertEqual(self.a.directionToEnum((1.0,0)),self.a.DIRECT_X)
  def test_reversex(self):
    self.assertEqual(self.a.directionToEnum((-1.0,0)),self.a.REVERSE_X)
  def test_directy(self):
    self.assertEqual(self.a.directionToEnum((0,1.0)),self.a.DIRECT_Y)
  def test_reversey(self):
    self.assertEqual(self.a.directionToEnum((0,-1.0)),self.a.REVERSE_Y)

if __name__ == '__main__':
    unittest.main()
